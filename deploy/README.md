Установка pip

```
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
```

Установка необходимых пакетов

```
pip3 install -r requirements.txt
```

Создание каталогов и создание первой конфигурации

```
./setup.sh
```
